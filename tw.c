// COMP2521 21T2 Assignment 1
// tw.c ... compute top N most frequent words in file F
// Usage: ./tw [Nwords] File

#include <assert.h>
#include <ctype.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "Dict.h"
#include "stemmer.h"
#include "WFreq.h"

#define MAXLINE 1000
#define MAXWORD 100

#define isWordChar(c) (isalnum(c) || (c) == '\'' || (c) == '-')

// add function prototypes for your own functions here
void wordToLower(char *word);  //lowercases word
bool notStopword(char *word, int n);  //If it is not a stopword, return true
bool isWordCharFunction(char *word); //Function based on isWordChar(c)
char* MallocWord(char w[]); //returns a pointer to the copy of the inserted word array


int main(int argc, char *argv[]) {
    int nWords;      // number of top frequency words to show
    char *fileName;  // name of file containing book text
	int MetaDataFlag = 0; //Flag used for excluding meta data
	WFreq wfs; //pointer to Wreq struct 

    // process command-line args
    switch (argc) {
        case 2:
            nWords = 10;
            fileName = argv[1];
            if (fileName == NULL) {
                fprintf(stderr, "Can't open %s\n", fileName);
                exit(EXIT_FAILURE);
            }
            break;
        case 3:
            nWords = atoi(argv[1]);
            if (nWords < 10)
                nWords = 10;
            fileName = argv[2];
            if (fileName == NULL) {
                fprintf(stderr, "Can't open %s\n", fileName);
                exit(EXIT_FAILURE);
            }
            break;
        default:
            fprintf(stderr, "Usage: %s [Nwords] File\n", argv[0]);
            exit(EXIT_FAILURE);
    }


    //Create stopwords dictionary
    Dict stopwordsDict = DictNew(); 
    FILE *fpp = fopen("stopwords", "r");

    if (fpp == NULL) {
        fprintf(stderr, "Can't open stopwords\n");
        exit(EXIT_FAILURE);
    }
    
    char stopword[MAXWORD];

    // Loop through strings in stopwords file
	while (fscanf(fpp, "%s", stopword) != EOF) {
        DictInsert(stopwordsDict, MallocWord(stopword));
    }

    fclose(fpp);


    //Create a dictionary for file 
    Dict d = DictNew();
    
    // Get file pointer
    FILE *fp = fopen(fileName, "r");

    // init line with to store lines of file 
    char line[MAXLINE + 1];
    char word[MAXLINE +1]; 

    int StartOfDetected = 0; //flag to detect if start of file line was detected before EOF
    int EndOfDetected = 0; //flag to detect if end of file line was detected before EOF

    //Loop through every line in the file and process words
    while (fgets(line, MAXLINE + 1, fp) != NULL) {
		if (strncmp(line, "*** END OF", 8) == 0) { 
			MetaDataFlag = 0; 
            EndOfDetected = 1; 
		}	
		if (MetaDataFlag == 1) {
            // Tokenise word 
            char ch = line[0]; 
            int i = 0; 
            int j = 0;
            while( ch != '\0') {
                if (isWordChar(ch)){
                    word[j] = line[i];
                    j++; 
                } else {
                    word[j] = '\0';
                    j = 0; 
                    //pass tokenised word to following functions 
                    wordToLower(word); 
                    int s = DictFind(stopwordsDict, word);
                    if (notStopword(word, s)) { 
					    stem(word, 0, strlen(word) -1);
					    DictInsert(d, MallocWord(word)); //have questions about this 
				    }  
                }
                i++; 
                ch = line[i];
            }
		}
		if (strncmp(line, "*** START OF", 10) == 0) {  
			MetaDataFlag = 1; 
            StartOfDetected = 1; 
		}
    }

    //Error handling - Detecting START OF/END OFF file 
    if (StartOfDetected == 0) {
        fprintf(stderr, "Not a Project Gutenberg book\n");
        exit(EXIT_FAILURE);
    }
    if (EndOfDetected == 0) {
        fprintf(stderr, "Not a Project Gutenberg book\n");
        exit(EXIT_FAILURE);
    }


    DictFindTopN(d, &wfs, nWords);
    
    DictFree(d); 
    DictFree(stopwordsDict);

	// Close file
    fclose(fp);
    
    return 0;
}

//Function based on isWordChar(c)
bool isWordCharFunction(char *word) {
	int a;
    for (a = 0; a < strlen(word); a++) {
        if(!isWordChar(word[a])) {
			return false; 
		}
    }	
	return true; 
}

//lowercases word
void wordToLower(char *word) {
    for (int i = 0; i < strlen(word); i++) {
        word[i] = tolower(word[i]);
    }
}

//If it is not a stopword found in stopwordsDict, return true 
bool notStopword(char *word, int n) {
	if (word == NULL) {
		return false; 
	} 
	if (strlen(word) < 2) {
		return false; 
	}
    
    if (n == 1) {
        return false; 
    }
    //Return true if word is not matched
    return true;
}

//returns a pointer to the copy of the inserted word array
char* MallocWord(char w[]) { 
    char* copy;
    copy = malloc(sizeof(char) * MAXWORD);
    
    strcpy(copy, w);
    return copy;
}




