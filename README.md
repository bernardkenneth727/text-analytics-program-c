# Text Analytics Program - C



## Project Description

This project uses data structures and algorithms in C to build a program that calculates the top N words in a text file, which is located in the same directory or repository. The files that I have implemented include tw.c and Dict.c while the remaining files were already provided. The Dict.c file utilises data structures such as binary search trees and recursion in functions that is used in the tw.c file (main file), which is used to calculate the top N words in a given text file. New text files may be added to the same repository or local directory in which the tw.c file is in. The program ignores common words, which is listed in the stopwords file. 

## Instructions: 

- Clone the repo locally and run the command ./tw [number] [file] 
- The number relates to the number of top words that is being searched for in the file. 

Example: ./tw 10 test.txt 

