// COMP2521 21T2 Assignment 1
// Dict.c ... implementation of the Dictionary ADT

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "Dict.h"
#include "WFreq.h"

typedef struct DictNode *Link; //have questions about this 
typedef struct WFreq *WF;

//DictNode struct used for each node
typedef struct DictNode {
   WF data;
   Link left;
   Link right;
} DictNode;

//Dictrep stuct used to represent the dictionary 
struct DictRep {
	Link root; //root note of BSTree
   int size; //number of nodes in entire dictionary 
};

// add function prototypes for your helper functions here
static Link DictInsertHelper(Link dictNode, char *word); 
static void DictFreeHelper(Link node);
static int DictFindHelper(Link node, char *word);
static void TreeInfixOrder(Link n);
static int comparator(const void *element1, const void *element2);
static void StoreDictAsArray(Link node, int *element, WFreq *allNodes); 


// Creates a new Dictionary
Dict DictNew(void) {
	Dict d = malloc(sizeof(*d));
 
   d->root = NULL; 
   d->size = 0;  

   return d;
}

// Frees the given Dictionary
void DictFree(Dict d) {
   if (d == NULL) {
		return; //didn't pass any root address to the function 
	} else {
      Link node = d->root; 
      free(d);
      DictFreeHelper(node);
   }
}

//Goes through the tree from the root node and frees memory
static void DictFreeHelper(Link node) {
   if (node->left == NULL && node->right == NULL) {
      free(node);
   } else if (node->left == NULL && node->right != NULL) {
      DictFreeHelper(node->right);
      free(node);
   } else if (node->left != NULL && node->right == NULL) {
      DictFreeHelper(node->left);
      free(node);
   } else if (node->left != NULL && node->right != NULL) {
      DictFreeHelper(node->left);
      DictFreeHelper(node->right);
      free(node);
   }
}

// Inserts an occurrence of the given word into the Dictionary
void DictInsert(Dict d, char *word) {
   if (word == NULL) {
      return; 
   }

   //dict doesn't exist so create dict and add node
   if (d == NULL) { 
      Dict d = DictNew(); 

      Link node = malloc(sizeof(*node));
      node->data = malloc(sizeof(struct WFreq));
      node->left = NULL;
      node->right = NULL;
      node->data->word = word;
      node->data->freq = 1;

      d->size++;  
      d->root = node; //root node added in dict 
   } else if (d->size == 0) { //dict exists but no node
      
      Link dictRoot = DictInsertHelper(d->root, word);
      d->root = dictRoot;
      d->size = 1;
   } else if (d->size > 0) { //if nodes exist in dictionary 
      
      if (DictFind(d, word) == 0) { //new word case 
         Link dictRoot = DictInsertHelper(d->root, word);
         d->root = dictRoot;
         d->size++;       
      } else {
         //word exists 
         DictInsertHelper(d->root, word);  
      }
   } else {
      return; 
   }
   
}

//Using recursion go through the BSTree and insert word and return root
static Link DictInsertHelper(Link dictNode, char *word) { 
   //word doesn't exist - creating new node
   if (dictNode == NULL) { 
      Link node = malloc(sizeof(*node));
      node->data = malloc(sizeof(struct WFreq));
      node->left = NULL;
      node->right = NULL;
      node->data->word = word;
      node->data->freq = 1;

      dictNode = node; 
   } else if (strcmp(word, dictNode->data->word) < 0) {
      dictNode->left = DictInsertHelper(dictNode->left, word);
   } else if (strcmp(word, dictNode->data->word) > 0) {
      dictNode->right = DictInsertHelper(dictNode->right, word);
   } else if (strcmp(word, dictNode->data->word) == 0) { //word exists - increase freq
      dictNode->data->freq++;
   }
   return dictNode; 
}


// Returns the occurrence count of the given word. Returns 0 if the word
// is not in the Dictionary.
int DictFind(Dict d, char *word) {
   Link root = d->root; 
   int wordFreq = DictFindHelper(root, word);

   return wordFreq; 
}


//recursively finds the relavant word and returns the frequency
static int DictFindHelper(Link node, char *word) { 
   int wordFreq;  

   if (node == NULL) {
      wordFreq = 0;
   } else if (strcmp(word, node->data->word) < 0) { 
      wordFreq = DictFindHelper(node->left, word); 
   } else if (strcmp(word, node->data->word) > 0) {
      wordFreq = DictFindHelper(node->right, word);
   } else if (strcmp(word, node->data->word) == 0) {
      wordFreq = node->data->freq;   
   } else {
   }
   return wordFreq;
}


// Finds  the top `n` frequently occurring words in the given Dictionary
// and stores them in the given  `wfs`  array  in  decreasing  order  of
// frequency,  and then in increasing lexicographic order for words with
// the same frequency. Returns the number of WFreq's stored in the given
// array (this will be min(`n`, #words in the Dictionary)) in  case  the
// Dictionary  does  not  contain enough words to fill the entire array.
// Assumes that the `wfs` array has size `n`.  
//Idea - traversal of the BSTree and store as an array in sorted order  
int DictFindTopN(Dict d, WFreq* wfs, int n) {
   //initialise index element for use in array 
   int element = 0;

   //create big array of all dictionary nodes
   WFreq *allNodes = malloc(sizeof(WFreq) * (d->size));
   //create array for DictFindTopN
	wfs = malloc(n * sizeof(WFreq));

   //Pass dictionary nodes into an array 
	StoreDictAsArray(d->root, &element, allNodes);

	//use qsort to sort array
   if (allNodes != NULL) {
      qsort(allNodes, d->size, sizeof(WFreq), comparator);
   } else {
      printf("Error: Dictionary array is null");
   }

   //copy first n nodes in allnodes to wfs array and print 
   if (allNodes != NULL && wfs != NULL) {
      for (int i = 0; i < n; i ++) {
         wfs[i] = allNodes[i];
	   }
      free(allNodes);
      for (int i = 0; i < n; i ++) {
         printf("%d %s\n", wfs[i].freq, wfs[i].word);
      }
   }

   //clears wfs array
   free(wfs);

   return n;
}

//Inorder traversal of dictionary nodes into array - lexographic order 
static void StoreDictAsArray(Link node, int *element, WFreq *allNodes) {
   if (node == NULL || allNodes == NULL) {
      return;
   }

	if (node->left != NULL) {
      StoreDictAsArray(node->left, element, allNodes);
   } 

   //stores data of nodes from left to right of tree 
	(allNodes[*element]).word = node->data->word;
	(allNodes[*element]).freq = node->data->freq;
   (*element) = (*element) + 1;

	if (node->right != NULL) {
      StoreDictAsArray(node->right, element, allNodes);
   } 
}

//rule for comparing and sorting for qsort function
static int comparator(const void *element1, const void *element2) {
	WFreq* node1 = (WFreq*) element1;
	WFreq* node2 = (WFreq*) element2;

   //Initialise and set values of differecnes in frequency and lexographic order
   int alphaVariance = strcmp(node1->word, node2->word);
	int FreqVariance = node2->freq - node1->freq;
   
   if (FreqVariance == 0) {
      return alphaVariance; 
   } else {
      return FreqVariance;
   }  
}


// Displays the given Dictionary. 
void DictShow(Dict d) {  

   TreeInfixOrder(d->root);
   printf("words: %d\n", d->size); 
}

//Prints dictionary nodes InOrder - lexographic order 
static void TreeInfixOrder(Link n) {
   if(n==NULL) {
      return; 
   } else {
      TreeInfixOrder(n->left);

      printf("(%s,%d)\n", n->data->word, n->data->freq);

      TreeInfixOrder(n->right);
   }    
} 




